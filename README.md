### Demo Instructions

1. Using a scratch Org with this master branch source code, source:push the code.

2. Show how Jitterbit can upload the JitterBitDemoData01.csv into the custom master detail object eAction and eAward
    a. Note:  each line in the csv contains repetitive master information for each detail record.  _Ext_Id fields are included in the data model and csv
	
3.  Show how Jitterbit can upload the .json files for the lookup related objects Broker and Property.  
	a.  Note: Compare how Jitterbit is superior to the sfdx comand such as 'force:data:tree:import'
	

    ```
